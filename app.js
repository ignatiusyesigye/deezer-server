const express = require('express');
const axios = require('axios');
const serverless = require('serverless-http');
const cors = require('cors');

const app = express();

app.use(cors());

// Routes
app.get('/search', async (req, res) => {
  const query = req.query.q;
  try {
    const response = await axios.get(
      `https://api.deezer.com/search?q=${query}`
    );
    res.json(response.data);
  } catch (error) {
    console.error('Error fetching search results:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

app.get('/artist/:id', async (req, res) => {
  const artistId = req.params.id;
  try {
    const artistResponse = await axios.get(
      `https://api.deezer.com/artist/${artistId}`
    );
    const topTracksResponse = await axios.get(
      `https://api.deezer.com/artist/${artistId}/top`
    );
    const albumsResponse = await axios.get(
      `https://api.deezer.com/artist/${artistId}/albums`
    );

    const artistInfo = artistResponse.data;
    const topTracks = topTracksResponse.data.data;
    const albums = albumsResponse.data.data;

    res.json({ artistInfo, topTracks, albums });
  } catch (error) {
    console.error('Error fetching artist data:', error);
    res.status(500).json({ error: 'Internal server error' });
  }
});

// Wrap the Express app with the serverless middleware
module.exports.handler = serverless(app);
